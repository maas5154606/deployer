#!/bin/bash

export HELM_EXPERIMENTAL_OCI=1

wait () {
  echo "Waiting ${1} seconds..."
  sleep $1
}

prepare_helm () {
  echo "Preparing helm"
  helm repo add istio https://istio-release.storage.googleapis.com/charts && \
    helm repo add hashicorp https://helm.releases.hashicorp.com && \
    helm repo update && \
    echo "Registering CRDS" && \
    kubectl apply -f ./maas-crds.yaml
}

install_istio () {
  if [ "$ISTIO_ENABLED" = "true" ]; then
    echo "Installing Istio"
    kubectl create namespace istio-system && \
      helm upgrade --install istio-base istio/base -n istio-system --set defaultRevision=default --wait && \
      helm upgrade --install istiod istio/istiod -n istio-system --wait
  fi
}

install_vault () {
  if [ "$HCV_ENABLED" = "true" ]; then
    echo "Deploying HC Vault"
    helm upgrade --install vault hashicorp/vault --values /opt/helm-vault-raft-values.yml --wait && \
      wait $HCV_WAIT_RUN_SEC
      kubectl exec vault-0 -- vault operator init \
      -key-shares=1 \
      -key-threshold=1 \
      -format=json > cluster-keys.json && \
      VAULT_UNSEAL_KEY=$(jq -r ".unseal_keys_b64[]" cluster-keys.json) && \
      VAULT_ROOT_TOKEN=$(jq -r ".root_token" cluster-keys.json) && \
      rm -f cluster-keys.json && \
      kubectl delete secret --ignore-not-found maas-hcv-token && \
      kubectl create secret generic maas-hcv-token \
        --from-literal=token=$VAULT_ROOT_TOKEN && \
      kubectl exec vault-0 -- vault operator unseal $VAULT_UNSEAL_KEY && \
      echo "Vault 0 unsealed" && \
      sleep 2 && \
      kubectl exec -ti vault-1 -- vault operator raft join http://vault-0.vault-internal:8200 && \
      echo "Vault 1 joined raft" && \
      sleep 2 && \
      kubectl exec -ti vault-2 -- vault operator raft join http://vault-0.vault-internal:8200 && \
      echo "Vault 2 joined raft" && \
      sleep 2 && \
      kubectl exec -ti vault-1 -- vault operator unseal $VAULT_UNSEAL_KEY && \
      echo "Vault 1 unsealed" && \
      sleep 2 && \
      kubectl exec -ti vault-2 -- vault operator unseal $VAULT_UNSEAL_KEY && \
      echo "Vault 2 unsealed"
      
    fi
    VAULT_ROOT_TOKEN=$( kubectl get secret maas-hcv-token -o jsonpath='{.data}' | jq -r ".token" | base64 -d )
    kubectl exec -ti vault-0 -- vault login $VAULT_ROOT_TOKEN && \
    kubectl exec -ti vault-0 -- vault auth enable -token-type=service jwt && \
    kubectl exec -ti vault-0 -- vault write auth/jwt/config bound_issuer="$IDP_ISSUER" jwks_url="$IDP_JWKS_URI" && \
    kubectl exec -ti vault-0 -- vault write auth/jwt/role/tenant bound_audiences=$IDP_AUDIENCE groups_claim=$IDP_TENANT_ID_CLAIM role_type=jwt user_claim=sub && \
    kubectl exec -ti vault-0 -- vault secrets enable kv
}

install_operator () {
  if [ "$OPERATOR_ENABLED" = "true" ]; then
    echo "Deploying MaaS Operator"
    helm repo add operator https://gitlab.com/api/v4/projects/54633744/packages/helm/stable
    helm upgrade --install \
      --set "idp.tenantIdClaim=${IDP_TENANT_ID_CLAIM}" \
      --set "idp.audience=${IDP_AUDIENCE}" \
      --set "idp.issuer=${IDP_ISSUER}" \
      --set "idp.jwksUri=${IDP_JWKS_URI}" \
      maas-operator \
      operator/maas-operator
    fi
}

install_workload_service () {
  if [ "$WORKLOAD_SERVICE_ENABLED" = "true" ]; then
    echo "Deploying MaaS Workload Service"
    helm repo add workload-service https://gitlab.com/api/v4/projects/54632282/packages/helm/stable
    helm upgrade --install \
      --set "idp.subPrefix=${IDP_SUB_PREFIX}" \
      --set "idp.tenantIdClaim=${IDP_TENANT_ID_CLAIM}" \
      maas-workload-service \
      workload-service/maas-workload-service
  fi
}

remove_deployer () {
  echo "Removing MaaS Deployer"
  kubectl delete deploy "${K8S_DEPLOY_NAME}"
}

echo "Initializing MaaS deployment"
prepare_helm
install_istio
install_vault
install_operator
install_workload_service
remove_deployer
echo "MaaS deployment complete"